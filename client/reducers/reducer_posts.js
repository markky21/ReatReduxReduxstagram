import { INCREMENT_LIKES} from '../actions/types';

function posts(state = [], action) {

    // console.log('action: ', action);

    switch (action.type) {
        case INCREMENT_LIKES:
            let newState = [...state];
            newState[action.index].likes++;
            return newState;
    }
    return state;

}

export default posts;