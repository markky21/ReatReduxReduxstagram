import {combineReducers} from 'redux';
import {routerReducer} from 'react-router-redux';

// reducers
import PostsReducer from './reducer_posts';
import CommentsReducer from './reducer_comments';

const rootReducer = combineReducers({
    posts: PostsReducer,
    comments: CommentsReducer,
    routing: routerReducer
})

export default rootReducer;