import {ADD_COMMENT, REMOVE_COMMENT} from '../actions/types';

function comments(state = {}, action) {

    const newState = {...state};

    switch (action.type) {
        case ADD_COMMENT:
            if (!newState[action.postId]) {
                newState[action.postId] = [];
            }
            newState[action.postId].push({text: action.comment, user: action.author})
            return newState;

        case REMOVE_COMMENT:
            newState[action.postId].splice(action.i, 1);
            return newState;
    }

    return state;

}

export default comments;