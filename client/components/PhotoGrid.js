import React, {Component} from 'react';

import Photo from './Photo';


class PhotoGrid extends Component {

    render() {
        return (
            <section className="photo-grid">
                {this.props.posts.map((post, i) => <Photo key={post.id} i={i} post={post} {...this.props}></Photo>)}
            </section>
        )
    }

}

export default PhotoGrid;