import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import * as actionsCreators from '../actions/actionsCreators';

import Main from './Main';

function mapStateToProps({posts, comments}) {
    return {
        posts,
        comments
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators(actionsCreators, dispatch);
}

const App = connect(mapStateToProps, mapDispatchToProps)(Main);
export default App;