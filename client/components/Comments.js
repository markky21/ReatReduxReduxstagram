import React, {Component} from 'react';

class Comments extends Component {

    renderComment(comment, i) {
        return (
            <div className='comment' key={i}>
                <p>
                    <strong>{comment.user}</strong>
                    {comment.text}
                    <button className="remove-comment" onClick={this.onRemoveClick.bind(this, i)}>&times;</button>
                </p>
            </div>
        );
    }

    onRemoveClick(i) {

        console.log('this: ', this);
        const postId = this.props.params.postId;
        this.props.removeComment(postId, i);

    }

    handleSubmit(e) {
        e.preventDefault();

        const {author, comment} = this.refs;
        const postId = this.props.params.postId;

        this.props.addComment(postId, author.value, comment.value);
        this.refs.commentForm.reset();
    }

    render() {

        const postComments = this.props.comments[this.props.params.postId] || [];

        return (
            <section>
                {postComments.map((comment, i) => this.renderComment(comment, i))}
                <form ref="commentForm" className='comment-form' onSubmit={this.handleSubmit.bind(this)}>
                    <input type="text" ref='author' placeholder='author'/>
                    <input type="text" ref='comment' placeholder='comment'/>
                    <input type="submit" hidden/>
                </form>
            </section>
        );
    }

}

export default Comments;