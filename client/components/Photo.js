import React, {Component} from 'react';
import {Link} from 'react-router';

import CSSTransitionGroup from 'react-addons-css-transition-group';

class Photo extends Component {

    onLikeClick() {
        this.props.increment(this.props.i);
    }

    render() {
        const post = this.props.post;
        const comments = this.props.comments;

        return (
            <article className='grid-figure'>
                <div className="grid-photo-wrap">
                    <Link to={`/view/${post.code}`}>
                        <img src={post.display_src} alt={post.caption} className='grid-photo'/>
                    </Link>

                    <CSSTransitionGroup
                        transitionName='like'
                        transitionEnterTimeout={500}
                        transitionLeaveTimeout={300}
                    >
                        <span key={post.likes} className='likes-heart'>
                            {post.likes}
                        </span>
                    </CSSTransitionGroup>

                    <figcaption>
                        <p>{post.caption}</p>
                        <div className='control-buttons'>
                            <button className='likes'
                                    onClick={this.onLikeClick.bind(this)}>&hearts; {post.likes}</button>
                            <Link to={`/view/${post.code}`} className='button'>
                                <span className="comment-count">
                                    <span className="speech-bubble"></span>
                                    {comments[post.code] ? comments[post.code].length : 0}
                                </span>
                            </Link>
                        </div>
                    </figcaption>

                </div>
            </article>
        );
    }

}

export default Photo;